import React from 'react';
import { Typography } from '@material-ui/core';
import styles from './DisplayMessage.module.css';

const DisplayMessage = ({ message, fullHeight = false, ...restProps }) => {
  return (
    <div
      className={
        fullHeight
          ? `${styles.displayContainer} ${styles.fullHeight}`
          : styles.displayContainer
      }
    >
      <Typography variant="subtitle1" color="error" {...restProps}>
        {message}
      </Typography>
    </div>
  );
};
export default DisplayMessage;
