import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import styles from './MoviesList.module.css';

const MoviesList = ({ movies }) => {
  return movies.map((movie) => {
    return (
      <Grid item sm={6} md={4} key={movie.id} className={styles.movieGridItem}>
        <Link
          to={`/home/movie/${movie.id}`}
          className={styles.moviePosterWrapper}
        >
          <img
            className={styles.moviePoster}
            src={`https://image.tmdb.org/t/p/w500${movie.poster_path}`}
            alt="movie-poster"
          />
        </Link>
        <Typography
          variant="body1"
          color="textPrimary"
          className={styles.movieTitle}
        >
          {movie.title}
        </Typography>
      </Grid>
    );
  });
};

export default MoviesList;
