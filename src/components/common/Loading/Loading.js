import React from 'react';
import { CircularProgress } from '@material-ui/core';
import styles from './Loading.module.css';

const Loading = ({ fullHeight = false, ...restProps }) => {
  return (
    <div
      className={
        fullHeight
          ? `${styles.loadingContainer} ${styles.fullHeight}`
          : styles.loadingContainer
      }
    >
      <CircularProgress
        color="primary"
        size="5rem"
        thickness={5}
        {...restProps}
      />
    </div>
  );
};
export default Loading;
