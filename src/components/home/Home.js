import React from 'react';
import MainSearch from '../search/MainSearch';

function Home() {
  return (
    <>
      <MainSearch />
    </>
  );
}

export default Home;
