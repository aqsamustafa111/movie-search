import React, { useState, useEffect } from 'react';
import { AppBar, Drawer, Button, Toolbar, IconButton } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Menu as MenuIcon } from '@material-ui/icons';
import { useDispatch, useSelector } from 'react-redux';
import { clearUser } from '../../redux/actions/userActions';
import { clearSessionId } from '../../redux/actions/sessionIdActions';
import movieDBLogo from '../../assets/images/movie_db_logo.svg';
import styles from './Navigation.module.css';

const navOptions = ['Dashboard', 'Favorites'];

function Navigation() {
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.user);
  const [mobileView, setMobileView] = useState(false);
  const [drawer, setDrawer] = useState(false);
  const [activeTab, setActiveTab] = useState(-1);

  useEffect(() => {
    const setResponsiveness = () => {
      return window.innerWidth < 1250
        ? setMobileView(true)
        : setMobileView(false);
    };

    setResponsiveness();
    window.addEventListener('resize', () => setResponsiveness());

    return () => {
      window.removeEventListener('resize', () => setResponsiveness());
    };
  }, []);

  const toggleDrawer = (value) => {
    setDrawer(value);
  };

  const clearActions = () => {
    return new Promise((resolve) => {
      dispatch(clearUser());
      dispatch(clearSessionId());
      setActiveTab(-1);
      setDrawer(false);
      resolve();
    });
  };

  const logoutHandler = () => {
    clearActions().then(() => {
      console.log('cleared');
    });
  };

  const navOptionsList = navOptions.map((navOption, index) => (
    <Button
      className={
        activeTab === index
          ? `${styles.navOption} ${styles.navOptionActive}`
          : styles.navOption
      }
      component={Link}
      to={`/${navOption.toLowerCase()}`}
      key={navOption}
      onClick={() => setActiveTab(index)}
    >
      {navOption}
    </Button>
  ));

  const NavOptionWrapper = () => (
    <div
      className={
        mobileView ? styles.navOptionsMobileWrapper : styles.navOptionsWrapper
      }
    >
      {navOptionsList}
      <Button variant="contained" color="primary" onClick={logoutHandler}>
        Log out
      </Button>
    </div>
  );

  if (!token) {
    return (
      <AppBar position="static">
        <img
          src={movieDBLogo}
          alt="movie db logo"
          className={styles.guestLogo}
        />
      </AppBar>
    );
  }

  return (
    <AppBar position="static">
      <Toolbar className={styles.toolbar}>
        <Link to="/home" onClick={() => setActiveTab(-1)}>
          <img
            width="100px"
            height="50px"
            src={movieDBLogo}
            alt="movie db logo"
          />
        </Link>

        {!mobileView ? (
          <NavOptionWrapper />
        ) : (
          <IconButton edge="end" color="primary">
            <MenuIcon
              onClick={() => {
                toggleDrawer(true);
              }}
            />
          </IconButton>
        )}

        <Drawer
          anchor="right"
          open={drawer}
          onClose={() => {
            toggleDrawer(false);
          }}
        >
          <NavOptionWrapper />
        </Drawer>
      </Toolbar>
    </AppBar>
  );
}

export default Navigation;
