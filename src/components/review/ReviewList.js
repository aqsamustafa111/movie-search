import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import ReviewContent from './ReviewContent';
import styles from './review.module.css';

const ReviewList = ({ reviews }) => {
  return reviews.map((review) => {
    return (
      <Grid key={review.id} className={styles.reviewContainer}>
        <Grid container>
          <Grid item xs={6}>
            <Typography variant="h6" gutterBottom>
              {review.author}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="h6" align="right" gutterBottom>
              {review.author_details.rating &&
                `Rating: ${review.author_details.rating}/10`}
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <ReviewContent {...review} />
        </Grid>
      </Grid>
    );
  });
};

export default ReviewList;
