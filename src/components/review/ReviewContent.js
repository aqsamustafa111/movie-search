import React, { useEffect, useState } from 'react';
import { Typography, Button } from '@material-ui/core';
import styled, { css } from 'styled-components';
import styles from './review.module.css';

const MAX_LIMIT = 100;

const Content = styled(Typography)`
  ${(props) => {
    return (
      props.truncate &&
      css`
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 4;
        -webkit-box-orient: vertical;
      `
    );
  }}
`;

const ReviewContent = ({ content: reviewContent }) => {
  const [truncate, setTruncate] = useState(false);
  const [buttonLabel, setButtonLabel] = useState(null);

  const contentHandler = () => {
    if (truncate) {
      setTruncate(false);
      setButtonLabel('See Less');
    } else {
      setTruncate(true);
      setButtonLabel('See More');
    }
  };

  const shouldContentTruncate = () => {
    return reviewContent.split(' ').length > MAX_LIMIT;
  };

  useEffect(() => {
    if (shouldContentTruncate()) {
      setTruncate(true);
      setButtonLabel('See More');
    } else {
      setTruncate(false);
    }
  }, []);

  return (
    <>
      <Content
        variant="body1"
        color="textSecondary"
        gutterBottom
        truncate={truncate}
      >
        {reviewContent}
      </Content>

      {shouldContentTruncate() && (
        <div className={styles.seeMoreButtonView}>
          <Button color="primary" onClick={contentHandler}>
            {buttonLabel}
          </Button>
        </div>
      )}
    </>
  );
};

export default ReviewContent;
