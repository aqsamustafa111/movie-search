import React from 'react';
import { Pie } from 'react-chartjs-2';

function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';

  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

const PieChart = ({ data }) => {
  const state = {
    labels: Object.keys(data),
    datasets: [
      {
        label: 'Rainfall',
        data: Object.values(data),
        backgroundColor: Object.values(data).map(() => getRandomColor()),
      },
    ],
  };
  return (
    <Pie
      data={state}
      options={{
        plugins: {
          legend: {
            display: true,
            position: 'right',
            labels: {
              color: 'white',
            },
          },
        },
        radius: '100%',
      }}
    />
  );
};

export default PieChart;
