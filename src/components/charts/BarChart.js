import React, { memo } from 'react';
import { Bar } from 'react-chartjs-2';
import colors from '../../colors';

const { PRIMARY, SECONDARY } = colors;

const BarChart = ({ dataset1, dataset2, chartTitle }) => {
  const state = {
    labels: Object.keys(dataset1.data),
    datasets: [
      {
        label: dataset1.datasetLabel,
        data: Object.values(dataset1.data),
        backgroundColor: '#e442b3',
        xAxisID: 'x',
        yAxisID: 'y',
      },
      {
        label: dataset2.datasetLabel,
        data: Object.values(dataset2.data),
        backgroundColor: PRIMARY,
        xAxisID: 'x',
        yAxisID: 'y',
      },
    ],
  };
  return (
    <Bar
      data={state}
      options={{
        scales: {
          y: {
            ticks: {
              beginAtZero: true,
              color: SECONDARY,
            },
            grid: {
              color: `${SECONDARY}40`,
            },
          },

          x: {
            ticks: {
              color: SECONDARY,
            },
            grid: {
              color: `${SECONDARY}40`,
            },
          },
        },
        plugins: {
          legend: {
            display: false,
            position: 'right',
            labels: {
              color: 'white',
            },
          },
          title: {
            display: true,
            text: chartTitle,
            fontSize: 20,
          },
        },
      }}
    />
  );
};

function areEqual(prevProps, nextProps) {
  const {
    dataset1: prevDataset1,
    dataset2: prevDataset2,
    chartTitle: prevChartTitle,
  } = prevProps;
  const {
    dataset1: nextDataset1,
    dataset2: nextDataset2,
    chartTitle: nextChartTitle,
  } = nextProps;

  if (prevChartTitle !== nextChartTitle) {
    return false;
  }

  if (prevDataset1.datasetLabel !== nextDataset1.datasetLabel) {
    return false;
  }

  if (prevDataset2.datasetLabel !== nextDataset2.datasetLabel) {
    return false;
  }

  let keys = Object.keys(prevDataset1.data);
  for (let i = 0; i < keys.length; i += 1) {
    const key = keys[i];
    if (prevDataset1.data[key] !== nextDataset1.data[key]) {
      return false;
    }
  }

  keys = Object.keys(prevDataset2.data);
  for (let i = 0; i < keys.length; i += 1) {
    const key = keys[i];
    if (prevDataset2.data[key] !== nextDataset2.data[key]) {
      return false;
    }
  }
  return true;
}

export default memo(BarChart, areEqual);
