import React, { memo } from 'react';
import { Line } from 'react-chartjs-2';

import colors from '../../colors';

const { PRIMARY, SECONDARY } = colors;

const LineChart = ({ data, chartTitle, datasetLabel }) => {
  const state = {
    labels: Object.keys(data),
    datasets: [
      {
        label: datasetLabel,
        data: Object.values(data),
        backgroundColor: PRIMARY,
        borderColor: '#e442b3',
        xAxisID: 'x',
        yAxisID: 'y',
      },
    ],
  };
  return (
    <Line
      data={state}
      options={{
        scales: {
          y: {
            ticks: {
              beginAtZero: true,
              color: SECONDARY,
            },
            grid: {
              color: `${SECONDARY}40`,
            },
          },

          x: {
            ticks: {
              color: SECONDARY,
            },
            grid: {
              color: `${SECONDARY}40`,
            },
          },
        },
        plugins: {
          legend: {
            display: false,
            position: 'right',
            labels: {
              color: 'white',
            },
          },
          title: {
            display: true,
            text: chartTitle,
            fontSize: 20,
          },
        },
      }}
    />
  );
};

export default memo(LineChart);
