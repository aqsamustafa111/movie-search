import React, { useState, useRef, useEffect } from 'react';
import { Container, Typography } from '@material-ui/core';
import SearchBar from './SearchBar';
import SearchOptions from './SearchOptions';
import SearchResults from './SearchResults';
import Loading from '../common/Loading/Loading';
import DisplayMessage from '../common/DisplayMessage/DisplayMessage';
import styles from './search.module.css';
import { search } from '../../actions';

const { getSearchQueryResults, getSearchOptionResults } = search;

const searchOptions = [
  {
    key: 'top_rated',
    value: 'Top Rated',
  },
  {
    key: 'popular',
    value: 'Popular',
  },
  {
    key: 'now_playing',
    value: 'Now Playing',
  },
];

const MainSearch = () => {
  const [searchResult, setSearchResult] = useState([]);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');
  const [currentQuery, setCurrentQuery] = useState('');
  const myRef = useRef(null);

  useEffect(() => {
    if (myRef && searchResult.length > 0) {
      myRef.current.scrollIntoView();
    }
  }, [loading]);

  const searchQueryHandler = async (searchQuery) => {
    setLoading(true);

    const response = await getSearchQueryResults(searchQuery);

    if (response.status === 200) {
      setSearchResult(response.data.results);
      setMessage('');
      setCurrentQuery(searchQuery);
    } else {
      setSearchResult([]);
      setMessage(response.data.status_message);
    }

    setLoading(false);
  };

  const searchOptionHandler = async (searchOption) => {
    setLoading(true);

    const response = await getSearchOptionResults(searchOption.key);

    if (response.status === 200) {
      setSearchResult(response.data.results);
      setMessage('');
      setCurrentQuery(searchOption.value);
    } else {
      setSearchResult([]);
      setMessage(response.data.status_message);
    }

    setLoading(false);
  };

  return (
    <div className={`container ${styles.mainContainer}`}>
      <Container className={styles.searchTopContainer} maxWidth="md">
        <Typography variant="h2" align="center">
          Search Movies Here!
        </Typography>
        <SearchBar searchQueryHandler={searchQueryHandler} />
        <SearchOptions
          data={searchOptions}
          searchOptionHandler={searchOptionHandler}
        />
      </Container>

      <div ref={myRef}>
        {!loading ? (
          <>
            {searchResult.length !== 0 && (
              <Container maxWidth="md">
                <Typography variant="h3" gutterBottom>
                  {`Search Results (${currentQuery})`}
                </Typography>
                {searchResult.length !== 0 && (
                  <SearchResults data={searchResult} />
                )}
              </Container>
            )}
            {message !== '' && <DisplayMessage message={message} />}
          </>
        ) : (
          <Loading />
        )}
      </div>
    </div>
  );
};

export default MainSearch;
