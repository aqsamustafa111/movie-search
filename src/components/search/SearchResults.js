import React from 'react';
import { Grid } from '@material-ui/core';
import MoviesList from '../common/MoviesList/MoviesList';

const SearchResults = ({ data }) => {
  return (
    <Grid container spacing={2}>
      <MoviesList movies={data} />
    </Grid>
  );
};

export default SearchResults;
