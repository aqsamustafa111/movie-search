import React from 'react';
import { Button } from '@material-ui/core';

const SearchOptions = ({ data, searchOptionHandler }) => {
  return (
    <div>
      {data.map((option) => (
        <Button
          color="secondary"
          onClick={() => {
            searchOptionHandler(option);
          }}
          key={option.key}
        >
          {option.value}
        </Button>
      ))}
    </div>
  );
};

export default SearchOptions;
