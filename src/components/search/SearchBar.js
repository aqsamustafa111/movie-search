import React, { useState } from 'react';
import { Button, TextField, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import styles from './search.module.css';

const SearchBar = ({ searchQueryHandler }) => {
  const [searchQuery, setSearchQuery] = useState('');
  return (
    <div className={styles.searchBar}>
      <TextField
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
        fullWidth
        label="Enter the name of the movie"
      />
      <Button
        onClick={() => {
          searchQueryHandler(searchQuery);
          setSearchQuery('');
        }}
        color="primary"
        size="large"
        endIcon={<SearchIcon />}
        className={styles.searchButton}
      >
        <Typography variant="h5">Search</Typography>
      </Button>
    </div>
  );
};

export default SearchBar;
