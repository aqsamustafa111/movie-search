import React from 'react';
import { Field } from 'formik';
import { TextField } from '@material-ui/core';

const Input = (props) => {
  const { label, name, ...rest } = props;

  return (
    <Field name={name}>
      {({ field, form }) => {
        return (
          <div>
            {/* <FormLabel>{label}</FormLabel> */}
            <TextField
              error={form.errors[name] && form.touched[name]}
              label={label}
              {...rest}
              {...field}
              helperText={form.errors[name]}
            />
          </div>
        );
      }}
    </Field>
  );
};

export default Input;
