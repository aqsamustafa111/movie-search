const colors = {
  PRIMARY: '#6E6568',
  SECONDARY: '#EAD3A8',
};

export default colors;
