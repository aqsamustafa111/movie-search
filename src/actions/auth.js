import axios from 'axios';

const auth = {
  login: (data) => {
    return axios
      .post('/api/signin', data)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  register: (data) => {
    return axios
      .post('/api/signup', data)
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
};

export default auth;
