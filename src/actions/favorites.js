import axios from 'axios';
import { MOVIE_DB_API_KEY, MOVIE_DB_BASE_URL } from '../config';

const favorites = {
  getFavoriteMovies: (userId, sessionId) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/account/${userId}/favorite/movies`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
          session_id: sessionId,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  addMovieToFavorites: (movieId, userId, sessionId) => {
    return axios
      .post(
        `${MOVIE_DB_BASE_URL}/account/${userId}/favorite`,
        {
          media_type: 'movie',
          media_id: parseInt(movieId),
          favorite: true,
        },
        {
          params: {
            api_key: MOVIE_DB_API_KEY,
            session_id: sessionId,
          },
        }
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  removeMovieFromFavorites: (movieId, userId, sessionId) => {
    return axios
      .post(
        `${MOVIE_DB_BASE_URL}/account/${userId}/favorite`,
        {
          media_type: 'movie',
          media_id: parseInt(movieId),
          favorite: false,
        },
        {
          params: {
            api_key: MOVIE_DB_API_KEY,
            session_id: sessionId,
          },
        }
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
};

export default favorites;
