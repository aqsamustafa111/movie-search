import axios from 'axios';
import { MOVIE_DB_API_KEY, MOVIE_DB_BASE_URL } from '../config';

const search = {
  getSearchQueryResults: (searchQuery) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/search/movie`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
          query: searchQuery,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  getSearchOptionResults: (searchOption) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/movie/${searchOption}`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
};

export default search;
