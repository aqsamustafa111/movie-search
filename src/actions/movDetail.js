import axios from 'axios';
import { MOVIE_DB_API_KEY, MOVIE_DB_BASE_URL } from '../config';

const movDetail = {
  getMovieDetails: (movieId) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/movie/${movieId}`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  getSimilarMovies: (movieId) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/movie/${movieId}/similar`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  getReviews: (movieId) => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/movie/${movieId}/reviews`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
};

export default movDetail;
