import axios from 'axios';
import { MOVIE_DB_API_KEY, MOVIE_DB_BASE_URL } from '../config';

const session = {
  createRequestToken: () => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/authentication/token/new`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
  addSessionIdToDatabase: ({ user, sessionId }) => {
    return axios
      .post('/api/update/sessionId', {
        user,
        sessionId,
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response;
      });
  },
};

export default session;
