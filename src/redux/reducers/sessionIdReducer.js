import { SET_SESSION_ID, CLEAR_SESSION_ID } from '../actions/sessionIdActions';

const initialState = {
  name: 'aksa',
  sessionId: null,
};

const sessionIdReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SESSION_ID:
      return { ...state, sessionId: action.sessionId };
    case CLEAR_SESSION_ID:
      return { ...state, sessionId: null };
    default:
      return state;
  }
};

export default sessionIdReducer;
