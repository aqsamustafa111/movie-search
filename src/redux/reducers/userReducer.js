import {
  CLEAR_USER,
  GET_USER,
  GET_USER_TOKEN,
  SET_USER,
} from '../actions/userActions';

const initialState = {
  user: null,
  token: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return state.user;
    case GET_USER_TOKEN:
      return state.token;
    case SET_USER:
      return { ...state, user: action.user, token: action.token, error: null };
    case CLEAR_USER:
      return { ...state, user: null, token: null, error: null };
    default:
      return state;
  }
};

export default userReducer;
