import axios from 'axios';
import { MOVIE_DB_API_KEY } from '../../config';

export const SET_SESSION_ID = 'SET_SESSION_ID';
export const CLEAR_SESSION_ID = 'CLEAR_SESSION_ID';

export const setSessionId = (sessionId) => {
  return {
    sessionId,
    type: SET_SESSION_ID,
  };
};

export const clearSessionId = () => {
  return {
    type: CLEAR_SESSION_ID,
  };
};

export const createSessionId = (requestToken) => {
  return async (dispatch) => {
    try {
      const session = await axios.post(
        `https://api.themoviedb.org/3/authentication/session/new?api_key=${MOVIE_DB_API_KEY}`,
        {
          request_token: requestToken,
        }
      );
      dispatch(setSessionId(session.data.session_id));
    } catch (err) {
      console.log('error while creating a new session id', err);
    }
  };
};
