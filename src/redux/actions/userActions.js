export const GET_USER = 'GET_USER';
export const GET_USER_TOKEN = 'GET_USER_TOKEN';
export const SET_USER = 'SET_USER';
export const SET_USER_ERROR = 'SET_USER_ERROR';
export const CLEAR_USER = 'CLEAR_USER';

export const getUser = () => {
  return {
    type: GET_USER,
  };
};

export const getUserToken = () => {
  return {
    type: GET_USER_TOKEN,
  };
};

export const setUser = ({ user, token }) => {
  return {
    user,
    token,
    type: SET_USER,
  };
};

export const clearUser = () => {
  return {
    type: CLEAR_USER,
  };
};
