import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {
  createTheme,
  ThemeProvider,
  responsiveFontSizes,
} from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/store';
import Navigation from './components/Navigation/Navigation';
import Login from './containers/auth/login/Login';
import Signup from './containers/auth/signup/Signup';
import Home from './components/home/Home';
import Landing from './containers/pages/Landing';
import MovieDetail from './containers/movieDetail/MovieDetail';
import FavoriteMovies from './containers/favorites/FavoriteMovies';
import Dashboard from './containers/dashboard/Dashboard';
import ProtectedRoute from './containers/pages/ProtectedRoute';
import colors from './colors';
import './App.css';

const { PRIMARY, SECONDARY } = colors;

let theme = createTheme({
  palette: {
    type: 'dark',
    primary: {
      main: PRIMARY,
    },
    secondary: {
      main: SECONDARY,
    },
    text: {
      primary: '#FFFFFF',
      secondary: SECONDARY,
    },

    contrastThreshold: 3,
  },
  typography: {
    h5: {
      color: PRIMARY,
    },
    h6: {
      color: PRIMARY,
    },
    h3: {
      color: PRIMARY,
    },
    button: {
      textTransform: 'capitalize',
    },
  },
});

theme = responsiveFontSizes(theme);

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router>
            <div className="App">
              <Navigation />
              <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={Signup} />
                <ProtectedRoute exact path="/home" component={Home} />
                <ProtectedRoute
                  exact
                  path="/home/movie/:id"
                  component={MovieDetail}
                />
                <ProtectedRoute
                  exact
                  path="/favorites"
                  component={FavoriteMovies}
                />
                <ProtectedRoute exact path="/dashboard" component={Dashboard} />
              </Switch>
            </div>
          </Router>
        </PersistGate>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
