import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  Grid,
  Container,
  Typography,
  Card,
  CardContent,
  Paper,
  Tabs,
  Tab,
} from '@material-ui/core';
import axios from 'axios';
import { favorites, movDetail } from '../../actions';
import { MOVIE_DB_API_KEY, MOVIE_DB_BASE_URL } from '../../config';
import PieChart from '../../components/charts/PieChart';
import DoughnutChart from '../../components/charts/DoughnutChart';
import BarChart from '../../components/charts/BarChart';
import LineChart from '../../components/charts/LineChart';
import Loading from '../../components/common/Loading/Loading';
import DisplayMessage from '../../components/common/DisplayMessage/DisplayMessage';
import styles from './Dashboard.module.css';

const { getFavoriteMovies } = favorites;
const { getMovieDetails } = movDetail;

const pieOptions = ['Pie', 'Doughnut'];

const Dashboard = () => {
  const { sessionId } = useSelector((state) => state.session);
  const { user } = useSelector((state) => state.user);
  const [favGenresCount, setFavGenresCount] = useState(null);
  const [favMoviesPopularity, setFavMoviesPopularity] = useState(null);
  const [pieOptionIndex, setPieOptionIndex] = useState(0);
  const [favMoviesBudget, setFavMoviesBudget] = useState(null);
  const [favMoviesRevenue, setFavMoviesRevenue] = useState(null);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('Not Available');

  const getGenres = () => {
    return axios
      .get(`${MOVIE_DB_BASE_URL}/genre/movie/list`, {
        params: {
          api_key: MOVIE_DB_API_KEY,
        },
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err.response ? err.response : err;
      });
  };

  const getFavMovies = async () => {
    const response = await getFavoriteMovies(user._id, sessionId);

    if (response.status === 200) {
      if (response.data.results.length === 0) {
        setMessage("You don't have any favorite movies yet.");
        throw Error('No favorites');
      }
      return response.data.results;
    }
    throw Error(response.data.status_message);
  };

  const getAllGenres = async () => {
    const genreResponse = await getGenres();

    if (genreResponse.status === 200) {
      const allGenres = {};

      genreResponse.data.genres.forEach((genre) => {
        allGenres[genre.id] = genre.name;
      });

      return allGenres;
    }
    throw genreResponse;
  };

  const getFavMoviesGenresCount = async () => {
    try {
      const allGenres = await getAllGenres();
      const favMovies = await getFavMovies();

      const genreCount = {};

      favMovies.forEach((fav) => {
        fav.genre_ids.forEach((id) => {
          const genreName = allGenres[id];

          if (!genreCount[genreName]) {
            genreCount[genreName] = 1;
          } else {
            genreCount[genreName] += 1;
          }
        });
      });

      return genreCount;
    } catch (err) {
      console.log('err', err);
      return undefined;
    }
  };

  const getFavMoviesPopularity = async () => {
    try {
      const favMovies = await getFavMovies();

      const tempPopularity = {};

      favMovies.forEach((fav) => {
        tempPopularity[fav.title] = fav.popularity;
      });

      return tempPopularity;
    } catch (err) {
      console.log('err', err);
      return undefined;
    }
  };

  const favmovieDetailPromises = (favMovies) => {
    return favMovies.map((fav) => getMovieDetails(fav.id));
  };

  const getBudgetRevenue = async () => {
    try {
      const favMovies = await getFavMovies();

      const favMoviesDetail = await Promise.all(
        favmovieDetailPromises(favMovies)
      );

      const tempBudget = {};
      const tempRevenue = {};

      favMoviesDetail.forEach((response) => {
        if (response.status === 200) {
          tempBudget[response.data.title] = response.data.budget;
          tempRevenue[response.data.title] = response.data.revenue;
        }
      });

      return { tempBudget, tempRevenue };
    } catch (err) {
      console.log('err', err);
      return { tempBudget: {}, tempRevenue: {} };
    }
  };

  useEffect(async () => {
    setLoading(true);

    const tempGenresCount = await getFavMoviesGenresCount();
    const tempPopularity = await getFavMoviesPopularity();
    const { tempBudget, tempRevenue } = await getBudgetRevenue();

    if (tempGenresCount) setFavGenresCount(tempGenresCount);
    if (tempPopularity) setFavMoviesPopularity(tempPopularity);
    if (Object.keys(tempBudget).length > 0) setFavMoviesBudget(tempBudget);
    if (Object.keys(tempRevenue).length > 0) setFavMoviesRevenue(tempRevenue);

    setLoading(false);
  }, []);

  const changePieOptionIndexHandler = (event, newValue) => {
    setPieOptionIndex(newValue);
  };

  const DoughnutPie = (props) => {
    return pieOptionIndex === 0 ? (
      <PieChart {...props} />
    ) : (
      <DoughnutChart {...props} />
    );
  };

  const renderPieOptions = pieOptions.map((pieOption) => (
    <Tab label={pieOption} key={pieOption} />
  ));

  return (
    <Container
      maxWidth="lg"
      className={`container ${styles.dashboardContainer}`}
    >
      <Typography variant="h2" gutterBottom align="center">
        Dashboard
      </Typography>

      {!loading ? (
        <Grid container spacing={2} alignItems="stretch">
          {favGenresCount && (
            <Grid item xs={12} md={5}>
              <Card className={styles.chartCardContainer} elevation={5} raised>
                <CardContent className={styles.chartCardContent}>
                  <Typography
                    variant="h5"
                    color="textPrimary"
                    gutterBottom
                    align="center"
                  >
                    Count of the Genres of your favorite movies
                  </Typography>
                  <div className={styles.chartView}>
                    <Paper square>
                      <Tabs
                        value={pieOptionIndex}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={changePieOptionIndexHandler}
                      >
                        {renderPieOptions}
                      </Tabs>
                    </Paper>
                    <DoughnutPie
                      data={favGenresCount}
                      chartTitle="Favorite Genre Count"
                    />
                  </div>
                </CardContent>
              </Card>
            </Grid>
          )}

          {favMoviesPopularity && (
            <Grid item xs={12} md={7}>
              <Card className={styles.chartCardContainer}>
                <CardContent className={styles.chartCardContent}>
                  <Typography
                    variant="h5"
                    color="textPrimary"
                    gutterBottom
                    align="center"
                  >
                    Popularity of your favorite movies
                  </Typography>
                  <div className={styles.chartView}>
                    <LineChart
                      data={favMoviesPopularity}
                      chartTitle="Popularity per movie"
                      datasetLabel="popularity"
                    />
                  </div>
                </CardContent>
              </Card>
            </Grid>
          )}

          {favMoviesBudget && favMoviesRevenue && (
            <Grid item xs={12}>
              <Card className={styles.chartCardContainer}>
                <CardContent className={styles.chartCardContent}>
                  <Typography
                    variant="h5"
                    color="textPrimary"
                    gutterBottom
                    align="center"
                  >
                    Budget and Revenue of your favorite movies
                  </Typography>
                  <div className={styles.chartView}>
                    <BarChart
                      dataset1={{
                        data: favMoviesBudget,
                        datasetLabel: 'Budget',
                      }}
                      dataset2={{
                        data: favMoviesRevenue,
                        datasetLabel: 'Revenue',
                      }}
                      chartTitle="Budget and Revenue of your favorite movies"
                    />
                  </div>
                </CardContent>
              </Card>
            </Grid>
          )}
        </Grid>
      ) : (
        <Loading fullHeight />
      )}

      {!loading &&
        !favGenresCount &&
        !favMoviesPopularity &&
        !favMoviesBudget &&
        !favMoviesRevenue && (
          <DisplayMessage
            message={message}
            fullHeight
            variant="h2"
            color="secondary"
          />
        )}
    </Container>
  );
};

export default Dashboard;
