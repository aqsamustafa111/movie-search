import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Container, Typography } from '@material-ui/core';
import { favorites } from '../../actions';
import MoviesList from '../../components/common/MoviesList/MoviesList';
import Loading from '../../components/common/Loading/Loading';
import DisplayMessage from '../../components/common/DisplayMessage/DisplayMessage';

const { getFavoriteMovies } = favorites;

const FavoriteMovies = () => {
  const { sessionId } = useSelector((state) => state.session);
  const { user } = useSelector((state) => state.user);
  const [favoriteMovies, setFavoriteMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState('');

  const getFavMovies = async () => {
    setLoading(true);

    const response = await getFavoriteMovies(user._id, sessionId);

    if (response?.status === 200) {
      if (response.data.results.length === 0) {
        setMessage("You don't have any favorites yet.");
      } else {
        setMessage('');
      }

      setFavoriteMovies(response.data.results);
    } else {
      setFavoriteMovies([]);

      if (response?.data?.status_message) {
        setMessage(response.data.status_message);
      } else {
        setMessage('An Error occurred while getting favorites');
      }
    }
    setLoading(false);
  };

  useEffect(() => {
    getFavMovies();
  }, []);

  return !loading ? (
    <Container maxWidth="md" className="container">
      {favoriteMovies.length > 0 && (
        <>
          <Typography variant="h2" gutterBottom align="center">
            Your Favorite Movies
          </Typography>
          <Grid container spacing={2}>
            <MoviesList movies={favoriteMovies} />
          </Grid>
        </>
      )}

      {message !== '' && (
        <DisplayMessage
          message={message}
          fullHeight
          variant="h5"
          color="secondary"
        />
      )}
    </Container>
  ) : (
    <Loading fullHeight />
  );
};

export default FavoriteMovies;
