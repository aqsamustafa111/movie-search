/* eslint-disable no-undef */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Container, Grid, Typography } from '@material-ui/core';
import {
  FavoriteBorderOutlined as FavoriteOutlinedIcon,
  Favorite as FavoriteIcon,
} from '@material-ui/icons';
import { useParams } from 'react-router-dom';
import queryString from 'query-string';
import { createSessionId } from '../../redux/actions/sessionIdActions';
import { setUser } from '../../redux/actions/userActions';
import MoviesList from '../../components/common/MoviesList/MoviesList';
import ReviewList from '../../components/review/ReviewList';
import Loading from '../../components/common/Loading/Loading';
import { movDetail, favorites, session } from '../../actions';
import styles from './MovieDetail.module.css';

const { getMovieDetails, getSimilarMovies, getReviews } = movDetail;

const { getFavoriteMovies, addMovieToFavorites, removeMovieFromFavorites } =
  favorites;

const { createRequestToken, addSessionIdToDatabase } = session;

const MovieDetail = (props) => {
  const params = useParams();
  const dispatch = useDispatch();
  const { sessionId } = useSelector((state) => state.session);
  const { user, token } = useSelector((state) => state.user);
  const [movieDetail, setMovieDetail] = useState({});
  const [similarMovies, setSimilarMovies] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [isFavorite, setIsFavorite] = useState(false);
  const [loading, setLoading] = useState(false);

  const isMovieFavorite = async () => {
    if (sessionId) {
      const response = await getFavoriteMovies(user._id, sessionId);

      if (response.status === 200) {
        const favoriteMovies = response.data.results;

        const result = favoriteMovies.filter(
          (favMovie) => favMovie.id === parseInt(params.id)
        );

        if (result.length > 0) {
          setIsFavorite(true);
        } else {
          setIsFavorite(false);
        }
      } else {
        console.log(response);
      }
    }
  };

  const getAllMovieDetails = async () => {
    setLoading(true);

    const resMovieDetails = await getMovieDetails(params.id);
    const resSimilarMovies = await getSimilarMovies(params.id);
    const resReviews = await getReviews(params.id);

    isMovieFavorite();

    if (resMovieDetails.status === 200) {
      setMovieDetail(resMovieDetails.data);
    } else {
      console.log(resMovieDetails);
    }

    if (resSimilarMovies.status === 200) {
      setSimilarMovies(resSimilarMovies.data.results.slice(0, 10));
    } else {
      console.log(resSimilarMovies);
    }

    if (resReviews.status === 200) {
      setReviews(resReviews.data.results.slice(0, 10));
    } else {
      console.log(resReviews);
    }

    setLoading(false);
  };

  const isRouteRedirect = () => {
    return props.location.search;
  };

  const getQueryParams = () => {
    return queryString.parse(props.location.search);
  };

  const createSessionIdAction = (requestToken) => {
    return new Promise((resolve) => {
      dispatch(createSessionId(requestToken));
      console.log('session id created in the action section');
      resolve();
    });
  };

  const processRouteRedirect = async () => {
    if (isRouteRedirect()) {
      const queryParams = getQueryParams();
      try {
        createSessionIdAction(queryParams.request_token);
        props.history.replace({
          pathname: `/home/movie/${params.id}`,
        });
      } catch (err) {
        console.log('session id cannot be created', err);
      }
    }
  };

  useEffect(() => {
    processRouteRedirect();
    getAllMovieDetails();
  }, [params.id]);

  const addToFavorites = async () => {
    const response = await addMovieToFavorites(params.id, user._id, sessionId);

    if (response.status >= 200 && response.status < 300) {
      console.log('response of adding movie favorites', response);
      setIsFavorite(true);
    } else {
      console.log(response);
    }
  };

  useEffect(async () => {
    if (sessionId && sessionId !== user.sessionId) {
      addToFavorites();

      const response = await addSessionIdToDatabase({ user, sessionId });

      if (response.status === 200) {
        console.log('sessionId added to database', response);
        dispatch(setUser({ user: { ...user, sessionId }, token }));
      } else {
        console.log(response);
      }
    }
  }, [sessionId]);

  const createAndAuthenticateRequestToken = async () => {
    try {
      const response = await createRequestToken();

      if (response.status === 200) {
        window.location.replace(
          `https://www.themoviedb.org/authenticate/${response.data.request_token}?redirect_to=http://localhost:3000/home/movie/${params.id}`
        );
      } else {
        console.log(response);
      }
    } catch (err) {
      console.log('error authenticating request token', err);
    }
  };

  const addFavoriteHandler = () => {
    console.log('add favorite handler');
    if (sessionId) {
      addToFavorites();
    } else {
      createAndAuthenticateRequestToken();
    }
  };

  const removeFromFavorites = async () => {
    const response = await removeMovieFromFavorites(
      params.id,
      user._id,
      sessionId
    );
    if (response.status >= 200 && response.status < 300) {
      console.log('response of removing movie favorites', response);
      setIsFavorite(false);
    } else {
      console.log(response);
    }
  };

  const removeFavoriteHandler = () => {
    console.log('remove favorite handler');
    removeFromFavorites();
  };

  const renderGenresList =
    Object.keys(movieDetail).length !== 0 &&
    movieDetail.genres.map((genre) => {
      return (
        <Grid item key={genre.id}>
          {genre.name}
        </Grid>
      );
    });

  return !loading ? (
    <Container maxWidth="md" className="container">
      <Grid container>
        <Grid item sm={4}>
          <img
            className={styles.moviePosterResult}
            src={`https://image.tmdb.org/t/p/w500${movieDetail.poster_path}`}
            alt="movie-poster"
          />
        </Grid>
        <Grid item sm={8} className={styles.titleAndSubDetailsView}>
          <Typography
            variant="h2"
            color="textSecondary"
            className={styles.titleText}
          >
            {movieDetail.title}
          </Typography>
          <Typography variant="h5" className={styles.titleText}>
            {movieDetail.tagline}
          </Typography>
          <Grid container className={styles.subDetailsView}>
            <Grid item xs={4}>
              {movieDetail.release_date}
            </Grid>
            <Grid item xs={4}>{`Rating: ${movieDetail.vote_average}`}</Grid>
            <Grid item xs={4}>
              {`${parseInt(parseInt(movieDetail.runtime) / 60)} h ${
                parseInt(movieDetail.runtime) % 60
              } m`}
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <div className={styles.lowerContainer}>
        <Typography
          variant="body1"
          color="textSecondary"
          className={styles.overview}
        >
          {movieDetail.overview}
        </Typography>
        <Grid container spacing={2} className={styles.genreView}>
          {renderGenresList}
        </Grid>
        <Button
          onClick={isFavorite ? removeFavoriteHandler : addFavoriteHandler}
          variant="contained"
          color="primary"
          endIcon={
            isFavorite ? (
              <FavoriteIcon color="error" />
            ) : (
              <FavoriteOutlinedIcon color="error" />
            )
          }
          className={styles.favoriteButton}
        >
          {isFavorite ? 'Remove it from favorite' : 'Add it to favorites'}
        </Button>
      </div>

      {similarMovies.length > 0 && (
        <div className={styles.lowerContainer}>
          <Typography variant="h3" gutterBottom>
            Similar Movies
          </Typography>
          <Grid container spacing={2}>
            <MoviesList movies={similarMovies} />
          </Grid>
        </div>
      )}

      {reviews.length > 0 && (
        <div className={styles.lowerContainer}>
          <Typography variant="h3" gutterBottom>
            Reviews
          </Typography>
          <Grid container>
            <ReviewList reviews={reviews} />
          </Grid>
        </div>
      )}
    </Container>
  ) : (
    <Loading fullHeight />
  );
};

export default MovieDetail;
