import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { Container, Button, Typography } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { setUser } from '../../../redux/actions/userActions';
import { setSessionId } from '../../../redux/actions/sessionIdActions';
import { auth } from '../../../actions';
import DisplayMessage from '../../../components/common/DisplayMessage/DisplayMessage';
import Loading from '../../../components/common/Loading/Loading';
import FormikControl from '../../../components/formikRoot/FormikControl';
import styles from '../auth.module.css';

const { login } = auth;

function Login(props) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [errMessage, setErrMessage] = useState('');

  const initialValues = {
    email: '',
    password: '',
  };

  const validationSchema = Yup.object({
    email: Yup.string().email('Invalid Email Format').required('Required'),
    password: Yup.string()
      .required('Required')
      .min(6, 'Password too small. Should be at least 6 character'),
  });

  // const loginUserAction = (data) => {
  //   return new Promise((resolve) => {
  //     dispatch(loginUser(data));
  //     resolve();
  //   });
  // };

  const onSubmit = async (data) => {
    setLoading(true);

    const response = await login(data);

    setLoading(false);

    if (response.status === 200) {
      dispatch(setUser(response.data));

      if (response.data.user.sessionId) {
        dispatch(setSessionId(response.data.user.sessionId));
      }

      setErrMessage('');
      props.history.push('/home');
    } else {
      console.log('sign in error', response.data);
      if (response.data.error) {
        setErrMessage(response.data.error);
      } else {
        setErrMessage(response.data);
      }
    }
  };

  return !loading ? (
    <Container maxWidth="sm" className={styles.container}>
      <Typography variant="h3" align="center">
        Login
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        className={styles.formContainer}
      >
        {() => {
          return (
            <Form className={styles.form}>
              <FormikControl
                control="input"
                name="email"
                type="email"
                label="Email"
                fullWidth
              />
              <FormikControl
                control="input"
                name="password"
                type="password"
                label="Password"
                fullWidth
              />
              <Button
                variant="contained"
                color="primary"
                size="large"
                className={styles.submitButton}
                type="submit"
              >
                Login
              </Button>
            </Form>
          );
        }}
      </Formik>
      <DisplayMessage message={errMessage} variant="h5" />
      <Typography variant="body1">
        {"Don't have an account, "}
        <Link to="/signup" replace>
          Signup
        </Link>
      </Typography>
    </Container>
  ) : (
    <Loading fullHeight />
  );
}

export default Login;
