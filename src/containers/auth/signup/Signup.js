import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { Link } from 'react-router-dom';
import { Container, Button, Typography } from '@material-ui/core';
import { auth } from '../../../actions';
import Loading from '../../../components/common/Loading/Loading';
import DisplayMessage from '../../../components/common/DisplayMessage/DisplayMessage';
import FormikControl from '../../../components/formikRoot/FormikControl';
import styles from '../auth.module.css';

const { register } = auth;

function Signup(props) {
  const [loading, setLoading] = useState(false);
  const [errMessage, setErrMessage] = useState('');

  const initialValues = {
    fullName: '',
    email: '',
    password: '',
  };

  const validationSchema = Yup.object({
    fullName: Yup.string().required('Required'),
    email: Yup.string().email('Invalid Email Format').required('Required'),
    password: Yup.string()
      .required('Required')
      .min(6, 'Password too small. Should be at least 6 character'),
  });

  const onSubmit = async (data) => {
    setLoading(true);

    const response = await register(data);

    setLoading(false);
    if (response.status === 200) {
      setErrMessage('');
      props.history.push('/');
    } else {
      console.log('sign up error', response.data);
      if (response.data.error) {
        setErrMessage(response.data.error);
      } else {
        setErrMessage(response.data);
      }
    }
  };

  return !loading ? (
    <Container maxWidth="sm" className={styles.container}>
      <Typography variant="h3" align="center">
        Signup
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
        className={styles.formContainer}
      >
        {() => {
          return (
            <Form className={styles.form}>
              <FormikControl
                control="input"
                name="fullName"
                type="text"
                label="Full Name"
                fullWidth
              />
              <FormikControl
                control="input"
                name="email"
                type="email"
                label="Email"
                fullWidth
              />
              <FormikControl
                control="input"
                name="password"
                type="password"
                label="Password"
                fullWidth
              />

              <Button
                variant="contained"
                color="primary"
                size="large"
                className={styles.submitButton}
                type="submit"
              >
                Signup
              </Button>
            </Form>
          );
        }}
      </Formik>
      <DisplayMessage message={errMessage} variant="h5" />
      <Typography variant="body1">
        {'Already have an account, '}
        <Link to="/login" replace>
          Login
        </Link>
      </Typography>
    </Container>
  ) : (
    <Loading fullHeight />
  );
}

export default Signup;
