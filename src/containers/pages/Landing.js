import React from 'react';

import { Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Landing = () => {
  const { token } = useSelector((state) => state.user);

  if (token) {
    return <Redirect to="/home" />;
  }
  return <Redirect to="/login" />;
};

export default Landing;
