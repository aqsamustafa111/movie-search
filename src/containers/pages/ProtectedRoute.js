import React from 'react';

import { Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';

const ProtectedRoute = ({ component: Component, ...restOfProps }) => {
  const { token } = useSelector((state) => state.user);

  if (token) {
    return (
      <Route {...restOfProps} render={(props) => <Component {...props} />} />
    );
  }
  return <Redirect to="/login" />;
};

export default ProtectedRoute;
